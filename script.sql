-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  mar. 05 déc. 2017 à 21:05
-- Version du serveur :  10.1.28-MariaDB
-- Version de PHP :  7.0.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `suivi_stage`
--

--
-- Déchargement des données de la table `classroom`
--

INSERT INTO `classroom` (`id`, `entitled`) VALUES
(1, 'BTS1'),
(2, 'BTS2'),
(3, 'Licence');

--
-- Déchargement des données de la table `company`
--

INSERT INTO `company` (`id`, `company_type_id`, `name`, `turnover`, `adress`, `adressCity`, `adressCP`, `adressComplement`, `phone`) VALUES
(1, 1, 'Capcom', 60290, 'Japon', 'Tokyo', '61654964', 'Aki', 54652),
(2, 2, 'entreprise', 100000, 'aa', 'aa', 'aa', 'aa', 5151);

--
-- Déchargement des données de la table `company_type`
--

INSERT INTO `company_type` (`id`, `entitled`) VALUES
(1, 'SSII'),
(2, 'Agence web');

--
-- Déchargement des données de la table `internship`
--

INSERT INTO `internship` (`id`, `company_id`, `peda_id`, `student_id`, `technical_id`, `begin`, `end`, `description`) VALUES
(1, 1, 1, 1, 1, '2017-12-05 00:00:00', '2018-01-26 00:00:00', 'Développement de monster hunter:world');

--
-- Déchargement des données de la table `internship_technology`
--

INSERT INTO `internship_technology` (`internship_id`, `technology_id`) VALUES
(1, 1);

--
-- Déchargement des données de la table `peda_ref`
--

INSERT INTO `peda_ref` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `confirmation_token`, `password_requested_at`, `roles`, `FirstName`, `LastName`, `Phone`, `Gender`) VALUES
(1, 'admin', 'admin', 'admin@admin.fr', 'admin@admin.fr', 1, NULL, '$2y$13$HpZGpOMYNre7t2vDMm1uNOmDdjLm5Ib/Xj5PyCpRbkyM5X7/A4GXm', '2017-12-05 20:51:00', NULL, NULL, 'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}', 'Prénom d\'admin', 'Nom d\'admin', '604060406', 1);

--
-- Déchargement des données de la table `promo`
--

INSERT INTO `promo` (`id`, `year`) VALUES
(1, 1995),
(2, 2017);

--
-- Déchargement des données de la table `student`
--

INSERT INTO `student` (`id`, `adress`, `adressCity`, `adressCp`, `adressComplement`, `Email`, `FirstName`, `LastName`, `Phone`, `Gender`) VALUES
(1, '155 Rue Neuve', 'Neuilly Sous Clermont', '60290', '15', 'r.dhersignerie@gmail.com', 'Ronan', 'Dhersignerie', '555', 1),
(2, 'une adresse', 'Une ville', '60290', 'complement', 'Charles@mail.com', 'Charles', 'Haller', '555555', 1),
(3, 'a', 'a', 'aa', 'a', 'knoor@hugo.soupe', 'Hugo', 'Knorr', '444', 1);

--
-- Déchargement des données de la table `student_promo_classroom`
--

INSERT INTO `student_promo_classroom` (`id`, `classroom_id`, `promo_id`, `student_id`) VALUES
(1, 3, 1, 1);

--
-- Déchargement des données de la table `technical_ref`
--

INSERT INTO `technical_ref` (`id`, `company_id`, `Email`, `FirstName`, `LastName`, `Phone`, `Gender`) VALUES
(1, 1, 'Kaname.Fujioka@capcom.jp', 'Kaname', 'Fujioka', '58585228', 1);

--
-- Déchargement des données de la table `technology`
--

INSERT INTO `technology` (`id`, `entitled`) VALUES
(1, 'MT framework'),
(2, 'php');

--
-- Déchargement des données de la table `visit`
--

INSERT INTO `visit` (`id`, `stage_id`, `date`, `observation`) VALUES
(1, 1, '2017-12-14 00:00:00', 'Super bon stage très instructif');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
