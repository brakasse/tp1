<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Classroom;
use AppBundle\Form\ClassroomType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;


class ClassroomController extends Controller
{
    /**
     * @Security("has_role('ROLE_ADMIN')")
     * @Route("/Classroom/add", name="Classroom_add")
     */
    public function addAction(Request $request){

        $classroom = new Classroom();

        $form = $this->createForm(ClassroomType::class, $classroom);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $classroom = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($classroom);
            $em->flush();

            return $this->redirectToRoute('homepage');
        }
        return $this->render('Classroom/add.html.twig', array('form' => $form->createView()));
    }

    /**
     * @Route("/Classroom/edit/{id}", name="Classroom_edit")
     */
    public function editAction(Request $request, $id){

        $classroom = $this->getDoctrine()->getRepository(Classroom::class)->find($id);

        $form = $this->createForm(ClassroomType::class, $classroom);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $classroom = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($classroom);
            $em->flush();

            return $this->redirectToRoute('homepage');
        }
        return $this->render('Classroom/edit.html.twig', array('form' => $form->createView()));
    }

    /**
     * @Route("/Classroom/delete/{id}", name="Classroom_delete")
     */
    public function deleteAction(Request $request, $id){

        $classroom = $this->getDoctrine()->getRepository(Classroom::class)->find($id);

        $em = $this->getDoctrine()->getManager();
        $em->remove($classroom);
        $em->flush();

        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/searchclass", name="Classroom_search")
     */
    public function searchAction(Request $request){

        $class = new Classroom();
        $table =[];
        $form = $this->createForm(ClassroomType::class,$class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $data = $form->getData();

            $immatriculation = $data->getEntitled();

            $table = $this->getDoctrine()->getRepository(Classroom::class)->getstudentperclass($data->getEntitled());
            return $this->render('suivi/Search.html.twig', ['form' => $form->createView(), 'table' => $table]);
        }

        return $this->render('suivi/Search.html.twig', ['form' => $form->createView(), 'table' => $table]);

    }
}
