<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Company;
use AppBundle\Form\CompanyType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class CompanyController extends Controller
{
    /**
     * @Security("has_role('ROLE_ADMIN')")
     * @Route("/Company/add", name="Company_add")
     */
    public function addAction(Request $request){

        $company = new Company();

        $form = $this->createForm(CompanyType::class, $company);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $company = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($company);
            $em->flush();

            return $this->redirectToRoute('homepage');
        }
        return $this->render('Company/add.html.twig', array('form' => $form->createView()));
    }

    /**
     * @Route("/Company/edit/{id}", name="Company_edit")
     */
    public function editAction(Request $request, $id){

        $company = $this->getDoctrine()->getRepository(Company::class)->find($id);

        $form = $this->createForm(CompanyType::class, $company);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $company = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($company);
            $em->flush();

            return $this->redirectToRoute('homepage');
        }
        return $this->render('Company/edit.html.twig', array('form' => $form->createView()));
    }

    /**
     * @Route("/Company/delete/{id}", name="Company_delete")
     */
    public function deleteAction(Request $request, $id){

        $company = $this->getDoctrine()->getRepository(Company::class)->find($id);

        $em = $this->getDoctrine()->getManager();
        $em->remove($company);
        $em->flush();

        return $this->redirectToRoute('homepage');
    }
}
