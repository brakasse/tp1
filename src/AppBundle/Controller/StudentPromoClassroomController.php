<?php

namespace AppBundle\Controller;

use AppBundle\Entity\StudentPromoClassroom;
use AppBundle\Form\StudentPromoClassroomType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 *@Security("has_role('ROLE_ADMIN')")
 */
class StudentPromoClassroomController extends Controller
{
    /**
     * @Route("/spc/add", name="spc_add")
     */
    public function addAction(Request $request){

        $spc = new StudentPromoClassroom();

        $form = $this->createForm(StudentPromoClassroomType::class, $spc);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $spc = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($spc);
            $em->flush();

            return $this->redirectToRoute('homepage');
        }
        return $this->render('StudentPromoClassroom/add.html.twig', array('form' => $form->createView()));
    }
}
