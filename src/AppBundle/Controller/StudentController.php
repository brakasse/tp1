<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Student;
use AppBundle\Form\SearchStudentType;
use AppBundle\Form\StudentType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class StudentController
 * @package AppBundle\Controller
 * @Security("has_role('ROLE_ADMIN')")
 */
class StudentController extends Controller
{
    /**
     * @Route("/Student/add", name="Student_add")
     */
    public function addAction(Request $request){

        $student = new Student();

        $form = $this->createForm(StudentType::class, $student);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $student = $form->getData();
            dump($student);
            $em = $this->getDoctrine()->getManager();

            $em->persist($student);

            $em->flush();

            return $this->redirectToRoute('homepage');
        }
        return $this->render('Student/add.html.twig', array('form' => $form->createView()));
    }

    /**
     * @Route("/Student/edit/{id}", name="Student_edit")
     */
    public function editAction(Request $request, $id){

        $student = $this->getDoctrine()->getRepository(Student::class)->find($id);

        $form = $this->createForm(StudentType::class, $student);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $student = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($student);
            $em->flush();

            return $this->redirectToRoute('homepage');
        }
        return $this->render('Student/edit.html.twig', array('form' => $form->createView()));
    }

    /**
 * @Route("/Student/delete/{id}", name="Student_delete")
 */
    public function deleteAction(Request $request, $id){

        $student = $this->getDoctrine()->getRepository(Student::class)->find($id);

        $em = $this->getDoctrine()->getManager();
        $em->remove($student);
        $em->flush();

        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/Student/view/{id}", name="Student_view")
     */
    public function viewAction(Request $request, $id){

        $student = $this->getDoctrine()->getRepository(Student::class)->find($id);
        $data =$student->getId();

        $promo = $this->getDoctrine()->getRepository(Student::class)->getclassandpromo($data);

        $classe =$promo[0];

        return $this->render('Student/view.html.twig', ['student' => $student,'classe' => $classe]);
    }

    /**
     * @Route("/searchstudent", name="Student_search")
     */
    public function searchAction(Request $request){

        $student = new Student();
        $table =[];
        $form = $this->createForm(SearchStudentType::class, $student);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $data = $form->getData();

            $immatriculation = $data->getFirstName();

            $table = $this->getDoctrine()->getRepository(Student::class)->getstudentpername($data->getFirstName());

            return $this->render('student/Search.html.twig', ['form' => $form->createView(), 'table' => $table]);
        }

        return $this->render('student/Search.html.twig', ['form' => $form->createView(), 'table' => $table]);

    }
}
