<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Promo;
use AppBundle\Form\PromoType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 *@Security("has_role('ROLE_ADMIN')")
 */
class PromoController extends Controller
{
    /**
     * @Route("/Promo/add", name="Promo_add")
     */
    public function addAction(Request $request){

        $promo = new Promo();

        $form = $this->createForm(PromoType::class, $promo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $promo = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($promo);
            $em->flush();

            return $this->redirectToRoute('homepage');
        }
        return $this->render('Promo/add.html.twig', array('form' => $form->createView()));
    }

    /**
     * @Route("/Promo/edit/{id}", name="Promo_edit")
     */
    public function editAction(Request $request, $id){

        $promo = $this->getDoctrine()->getRepository(Promo::class)->find($id);

        $form = $this->createForm(PromoType::class, $promo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $promo = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($promo);
            $em->flush();

            return $this->redirectToRoute('homepage');
        }
        return $this->render('Promo/edit.html.twig', array('form' => $form->createView()));
    }

    /**
     * @Route("/Promo/delete/{id}", name="Promo_delete")
     */
    public function deleteAction(Request $request, $id){

        $promo = $this->getDoctrine()->getRepository(Promo::class)->find($id);

        $em = $this->getDoctrine()->getManager();
        $em->remove($promo);
        $em->flush();

        return $this->redirectToRoute('homepage');
    }
}
