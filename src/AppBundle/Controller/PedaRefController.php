<?php

namespace AppBundle\Controller;

use AppBundle\Entity\PedaRef;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Pedaref controller.
 * @Security("has_role('ROLE_ADMIN')")
 * @Route("pedagogic")
 */
class PedaRefController extends Controller
{
    /**
     * Lists all pedaRef entities.
     *
     * @Route("/", name="pedagogic_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $pedaRefs = $em->getRepository('AppBundle:PedaRef')->findAll();

        return $this->render('pedaref/index.html.twig', array(
            'pedaRefs' => $pedaRefs,
        ));
    }

    /**
     * Creates a new pedaRef entity.
     *
     * @Route("/new", name="pedagogic_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $pedaRef = new Pedaref();
        $form = $this->createForm('AppBundle\Form\PedaRefType', $pedaRef);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($pedaRef);
            $em->flush();

            return $this->redirectToRoute('pedagogic_show', array('id' => $pedaRef->getId()));
        }

        return $this->render('pedaref/new.html.twig', array(
            'pedaRef' => $pedaRef,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a pedaRef entity.
     *
     * @Route("/{id}", name="pedagogic_show")
     * @Method("GET")
     */
    public function showAction(PedaRef $pedaRef)
    {
        $deleteForm = $this->createDeleteForm($pedaRef);

        return $this->render('pedaref/show.html.twig', array(
            'pedaRef' => $pedaRef,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing pedaRef entity.
     *
     * @Route("/{id}/edit", name="pedagogic_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, PedaRef $pedaRef)
    {
        $deleteForm = $this->createDeleteForm($pedaRef);
        $editForm = $this->createForm('AppBundle\Form\PedaRefType', $pedaRef);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('pedagogic_edit', array('id' => $pedaRef->getId()));
        }

        return $this->render('pedaref/edit.html.twig', array(
            'pedaRef' => $pedaRef,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a pedaRef entity.
     *
     * @Route("/{id}", name="pedagogic_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, PedaRef $pedaRef)
    {
        $form = $this->createDeleteForm($pedaRef);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($pedaRef);
            $em->flush();
        }

        return $this->redirectToRoute('pedagogic_index');
    }

    /**
     * Creates a form to delete a pedaRef entity.
     *
     * @param PedaRef $pedaRef The pedaRef entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(PedaRef $pedaRef)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('pedagogic_delete', array('id' => $pedaRef->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
