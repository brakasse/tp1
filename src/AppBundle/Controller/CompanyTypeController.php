<?php

namespace AppBundle\Controller;

use AppBundle\Entity\CompanyType;
use AppBundle\Form\CompanyTypeType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Security("has_role('ROLE_ADMIN')")
 * Class CompanyTypeController
 * @package AppBundle\Controller
 */
class CompanyTypeController extends Controller
{
    /**

     * @Route("/CompanyType/add", name="CompanyType_add")
     */
    public function addAction(Request $request){

        $companytype = new CompanyType();

        $form = $this->createForm(CompanyTypeType::class, $companytype);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $companytype = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($companytype);
            $em->flush();

            return $this->redirectToRoute('homepage');
        }
        return $this->render('CompanyType/add.html.twig', array('form' => $form->createView()));
    }

    /**
     * @Route("/CompanyType/edit/{id}", name="CompanyType_edit")
     */
    public function editAction(Request $request, $id){

        $companytype = $this->getDoctrine()->getRepository(CompanyType::class)->find($id);

        $form = $this->createForm(CompanyTypeType::class, $companytype);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $companytype = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($companytype);
            $em->flush();

            return $this->redirectToRoute('homepage');
        }
        return $this->render('CompanyType/edit.html.twig', array('form' => $form->createView()));
    }

    /**
     * @Route("/Company/delete/{id}", name="Company_delete")
     */
    public function deleteAction(Request $request, $id){

        $companytype = $this->getDoctrine()->getRepository(CompanyType::class)->find($id);

        $em = $this->getDoctrine()->getManager();
        $em->remove($companytype);
        $em->flush();

        return $this->redirectToRoute('homepage');
    }
}
