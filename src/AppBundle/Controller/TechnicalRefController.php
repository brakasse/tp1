<?php

namespace AppBundle\Controller;

use AppBundle\Entity\TechnicalRef;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Technicalref controller.
 *
 * @Route("technical")
 * @Security("has_role('ROLE_ADMIN')")
 */
class TechnicalRefController extends Controller
{
    /**
     * Lists all technicalRef entities.
     *
     * @Route("/", name="technical_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $technicalRefs = $em->getRepository('AppBundle:TechnicalRef')->findAll();

        return $this->render('technicalref/index.html.twig', array(
            'technicalRefs' => $technicalRefs,
        ));
    }

    /**
     * Creates a new technicalRef entity.
     *
     * @Route("/new", name="technical_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $technicalRef = new Technicalref();
        $form = $this->createForm('AppBundle\Form\TechnicalRefType', $technicalRef);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($technicalRef);
            $em->flush();

            return $this->redirectToRoute('technical_show', array('id' => $technicalRef->getId()));
        }

        return $this->render('technicalref/new.html.twig', array(
            'technicalRef' => $technicalRef,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a technicalRef entity.
     *
     * @Route("/{id}", name="technical_show")
     * @Method("GET")
     */
    public function showAction(TechnicalRef $technicalRef)
    {
        $deleteForm = $this->createDeleteForm($technicalRef);

        return $this->render('technicalref/show.html.twig', array(
            'technicalRef' => $technicalRef,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing technicalRef entity.
     *
     * @Route("/{id}/edit", name="technical_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, TechnicalRef $technicalRef)
    {
        $deleteForm = $this->createDeleteForm($technicalRef);
        $editForm = $this->createForm('AppBundle\Form\TechnicalRefType', $technicalRef);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('technical_edit', array('id' => $technicalRef->getId()));
        }

        return $this->render('technicalref/edit.html.twig', array(
            'technicalRef' => $technicalRef,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a technicalRef entity.
     *
     * @Route("/{id}", name="technical_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, TechnicalRef $technicalRef)
    {
        $form = $this->createDeleteForm($technicalRef);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($technicalRef);
            $em->flush();
        }

        return $this->redirectToRoute('technical_index');
    }

    /**
     * Creates a form to delete a technicalRef entity.
     *
     * @param TechnicalRef $technicalRef The technicalRef entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TechnicalRef $technicalRef)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('technical_delete', array('id' => $technicalRef->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
