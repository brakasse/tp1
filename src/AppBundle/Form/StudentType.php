<?php

namespace AppBundle\Form;

use AppBundle\Entity\StudentGraduation;
use AppBundle\Repository\GraduationRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Entity\Graduation;

class StudentType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('adress', TextType::class, array('label' => 'Adresse'))
            ->add('adresscity', TextType::class, array('label' => 'Ville'))
            ->add('adresscp', TextType::class, array('label' => 'Code postale'))
            ->add('adresscomplement', TextType::class, array('label' => 'Complément'))
            ->add('firstname', TextType::class, array('label' => 'Prénom'))
            ->add('lastname', TextType::class, array('label' => 'Nom'))
            ->add('phone', IntegerType::class, array('label' => 'Numéro de téléphone'))
            ->add('email', TextType::class, array('label' => 'Adresse mail'))
            ->add('gender', ChoiceType::class, array(
                'choices' => array(
                    'Homme' => true,
                    'Femme' => false,
                )))
            ->add('graduation', CollectionType::class, [
                'entry_type' => StudentGraduationType::class,
                'allow_add' => true,
                'prototype' => true,
                'by_reference' => false,
            ]);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\Student'
        ]);
    }
}
