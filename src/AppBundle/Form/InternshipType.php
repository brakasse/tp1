<?php

namespace AppBundle\Form;

use AppBundle\Entity\Company;
use AppBundle\Entity\Student;
use AppBundle\Entity\Technology;
use AppBundle\Entity\TechnicalRef;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Entity\PedaRef;
use Symfony\Component\Validator\Constraints\Date;

class InternshipType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('begin', DateType::class,[
            'widget' => 'single_text', 'label' => 'Date de début'
        ])
            ->add('end', DateType::class,[
                'widget' => 'single_text', 'label' => 'Date de fin'
            ])
            ->add('description')

            ->add('company',EntityType::class,[
                'class' => Company::class,
                'choice_label' => 'name',
                'label' => 'Entreprise'
            ])
            ->add('peda',EntityType::class,[
                'class' => PedaRef::class, 'label' => 'Référent pédagogique'
            ])
            ->add('student', EntityType::class,[
                'class' => Student::class, 'label' => 'Elève'
            ])
            ->add('technical', EntityType::class,[
                'class' => TechnicalRef::class, 'label' => 'Référent technique'
            ])
            ->add('tech', EntityType::class,[
                'class' => Technology::class,
                'multiple' => true, 'label' => 'Technologie principale'
            ])
            ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Internship'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_internship';
    }


}
