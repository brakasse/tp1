<?php

namespace AppBundle\Form;

use AppBundle\Repository\CompanyTypeRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CompanyType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array('label' => 'Nom' ))
            ->add('turnover', IntegerType::class, array('label' => 'Chiffre d\'affaire' ))
            ->add('adress', TextType::class, array('label' => 'Adresse' ))
            ->add('adresscity', TextType::class, array('label' => 'Ville' ))
            ->add('adresscp', TextType::class, array('label' => 'Code Postale' ))
            ->add('adresscomplement', TextType::class, array('label' => 'Complément d\'adresse' ))
            ->add('phone', IntegerType::class, array('label' => 'Numéro de téléphone' ))
            ->add('companyType', EntityType::class, array('class' => \AppBundle\Entity\CompanyType::class,
            'choice_label' => 'entitled',
            'query_builder' => function (CompanyTypeRepository $er) {
                return $er->createQueryBuilder('c')
                    ->orderBy('c.entitled', 'ASC');
            },
            'label' => 'Type d\'entreprise'));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Company'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_company';
    }


}
