<?php

namespace AppBundle\Form;

use AppBundle\Entity\Classroom;
use AppBundle\Entity\Promo;
use AppBundle\Entity\Student;
use AppBundle\Repository\ClassRepository;
use AppBundle\Repository\PromoRepository;
use AppBundle\Repository\StudentRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StudentPromoClassroomType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('classroom', EntityType::class, array('class' => Classroom::class,
                'choice_label' => 'entitled',
                'query_builder' => function (ClassRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->orderBy('c.entitled', 'ASC');
                },
                'label' => 'Classe'))
            ->add('promo', EntityType::class, array('class' => Promo::class,
                'choice_label' => 'year',
                'query_builder' => function (PromoRepository $er) {
                    return $er->createQueryBuilder('p')
                        ->orderBy('p.year', 'ASC');
                },
                'label' => 'Année'))
            ->add('student', EntityType::class, array('class' => Student::class,
            'choice_label' => 'firstName',
            'query_builder' => function (StudentRepository $er) {
                return $er->createQueryBuilder('s')
                    ->orderBy('s.firstName', 'ASC');
            },
            'label' => 'Elève'));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\StudentPromoClassroom'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_studentpromoclassroom';
    }


}
