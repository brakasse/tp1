<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Traits\PersonTrait;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * PedaRef
 *
 * @ORM\Table(name="peda_ref")
 * @ORM\Entity
 */
class PedaRef extends BaseUser
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    use PersonTrait;
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return bool
     */
    public function isAdmin()
    {
        return $this->admin;
    }

    /**
     * @param bool $admin
     */
    public function setAdmin($admin)
    {
        $this->admin = $admin;
    }
    
    /**
     * @return string
     */
    public function  __toString()
    {
        return sprintf('%s %s',$this->lastName, $this->firstName);
    }

}

