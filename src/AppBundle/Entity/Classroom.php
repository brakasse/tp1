<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Classroom
 *
 * @ORM\Table(name="classroom")
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ClassRepository")
 */
class Classroom
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="entitled", type="string", length=255, nullable=false)
     */
    private $entitled;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Promo", mappedBy="idClassroom")
     */
    private $idPromo;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getEntitled()
    {
        return $this->entitled;
    }

    /**
     * @param string $entitled
     */
    public function setEntitled( $entitled)
    {
        $this->entitled = $entitled;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIdPromo()
    {
        return $this->idPromo;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $idPromo
     */
    public function setIdPromo(\Doctrine\Common\Collections\Collection $idPromo)
    {
        $this->idPromo = $idPromo;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idPromo = new \Doctrine\Common\Collections\ArrayCollection();
    }

}

