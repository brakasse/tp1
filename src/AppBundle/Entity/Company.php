<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Company
 *
 * @ORM\Table(name="company", indexes={@ORM\Index(name="FK_company_type_id_id", columns={"company_type_id"})})
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CompanyRepository")
 */
class Company
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
public function __toString()
{
    return sprintf('%s', $this->name);
}

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var float
     *
     * @ORM\Column(name="turnover", type="float", precision=10, scale=0, nullable=true)
     */
    private $turnover;

    /**
     * @var string
     *
     * @ORM\Column(name="adress", type="string", length=255, nullable=false)
     */
    private $adress;

    /**
     * @var string
     *
     * @ORM\Column(name="adressCity", type="string", length=255, nullable=false)
     */
    private $adresscity;

    /**
     * @var string
     *
     * @ORM\Column(name="adressCP", type="string", length=255, nullable=false)
     */
    private $adresscp;

    /**
     * @var string
     *
     * @ORM\Column(name="adressComplement", type="string", length=255, nullable=true)
     */
    private $adresscomplement;

    /**
     * @var integer
     *
     * @ORM\Column(name="phone", type="integer", nullable=true)
     */
    private $phone;

    /**
     * @var \AppBundle\Entity\CompanyType
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\CompanyType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="company_type_id", referencedColumnName="id")
     * })
     */
    private $companyType;


    /**
     * @param int $id
     */
    public function setId( $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName( $name)
    {
        $this->name = $name;
    }

    /**
     * @return float
     */
    public function getTurnover()
    {
        return $this->turnover;
    }

    /**
     * @param float $turnover
     */
    public function setTurnover( $turnover)
    {
        $this->turnover = $turnover;
    }

    /**
     * @return string
     */
    public function getAdress()
    {
        return $this->adress;
    }

    /**
     * @param string $adress
     */
    public function setAdress( $adress)
    {
        $this->adress = $adress;
    }

    /**
     * @return string
     */
    public function getAdresscity()
    {
        return $this->adresscity;
    }

    /**
     * @param string $adresscity
     */
    public function setAdresscity( $adresscity)
    {
        $this->adresscity = $adresscity;
    }

    /**
     * @return string
     */
    public function getAdresscp()
    {
        return $this->adresscp;
    }

    /**
     * @param string $adresscp
     */
    public function setAdresscp( $adresscp)
    {
        $this->adresscp = $adresscp;
    }

    /**
     * @return string
     */
    public function getAdresscomplement()
    {
        return $this->adresscomplement;
    }

    /**
     * @param string $adresscomplement
     */
    public function setAdresscomplement( $adresscomplement)
    {
        $this->adresscomplement = $adresscomplement;
    }

    /**
     * @return int
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param int $phone
     */
    public function setPhone( $phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return CompanyType
     */
    public function getCompanyType()
    {
        return $this->companyType;
    }

    /**
     * @param CompanyType $companyType
     */
    public function setCompanyType(CompanyType $companyType)
    {
        $this->companyType = $companyType;
    }


}

