<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * StudentGraduation
 *
 * @ORM\Table(name="student_graduation")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\StudentGraduationRepository")
 */
class StudentGraduation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="obtention", type="datetime")
     */
    private $obtention;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Student", inversedBy="graduation")
     */
    private $student;

    /**
     * @return mixed
     */
    public function getStudent()
    {
        return $this->student;
    }

    /**
     * @param mixed $student
     * @return StudentGraduation
     */
    public function setStudent($student)
    {
        $this->student = $student;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGraduation()
    {
        return $this->graduation;
    }

    /**
     * @param mixed $graduation
     * @return StudentGraduation
     */
    public function setGraduation($graduation)
    {
        $this->graduation = $graduation;
        return $this;
    }

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Graduation", inversedBy="student")
     */
    private $graduation;
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set obtention
     *
     * @param \DateTime $obtention
     *
     * @return StudentGraduation
     */
    public function setObtention($obtention)
    {
        $this->obtention = $obtention;

        return $this;
    }

    /**
     * Get obtention
     *
     * @return \DateTime
     */
    public function getObtention()
    {
        return $this->obtention;
    }
}

