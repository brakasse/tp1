<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Promo
 *
 * @ORM\Table(name="promo")
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PromoRepository")
 */
class Promo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param int $year
     */
    public function setYear($year)
    {
        $this->year = $year;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIdClassroom()
    {
        return $this->idClassroom;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $idClassroom
     */
    public function setIdClassroom($idClassroom)
    {
        $this->idClassroom = $idClassroom;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="year", type="integer", nullable=false)
     */
    private $year;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Classroom", inversedBy="idPromo")
     * @ORM\JoinTable(name="classroom_promo",
     *   joinColumns={
     *     @ORM\JoinColumn(name="id_promo", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="id_classroom", referencedColumnName="id")
     *   }
     * )
     */
    private $idClassroom;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idClassroom = new \Doctrine\Common\Collections\ArrayCollection();
    }
}


