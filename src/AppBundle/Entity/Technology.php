<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Technology
 *
 * @ORM\Table(name="technology")
 * @ORM\Entity
 */
class Technology
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="entitled", type="string", length=255, nullable=false)
     */
    private $entitled;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Internship", mappedBy="technology")
     */
    private $internship;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getEntitled()
    {
        return $this->entitled;
    }

    /**
     * @param string $entitled
     */
    public function setEntitled($entitled)
    {
        $this->entitled = $entitled;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInternship()
    {
        return $this->internship;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $internship
     */
    public function setInternship($internship)
    {
        $this->internship = $internship;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->internship = new \Doctrine\Common\Collections\ArrayCollection();
    }

//
//    /**
//     * @return string
//     */
//    public function getEntitled()
//    {
//        return $this->entitled;
//    }
//
//    /**
//     * @param string $entitled
//     */
//    public function setEntitled(string $entitled)
//    {
//        $this->entitled = $entitled;
//    }
//
//    /**
//     * @return \Doctrine\Common\Collections\Collection
//     */
//    public function getInternship(): \Doctrine\Common\Collections\Collection
//    {
//        return $this->internship;
//    }
//
//    /**
//     * @param \Doctrine\Common\Collections\Collection $internship
//     */
//    public function setInternship(\Doctrine\Common\Collections\Collection $internship)
//    {
//        $this->internship = $internship;
//    }
//
    public function __toString()
    {
        return sprintf('%s', $this->entitled);
    }
}

