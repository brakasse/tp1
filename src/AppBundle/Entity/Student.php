<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use AppBundle\Entity\Traits\PersonTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * Student
 *
 * @ORM\Table(name="student")
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Repository\StudentRepository")
 */
class Student

{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    use PersonTrait;
    /**
     * @var string
     *
     * @ORM\Column(name="adress", type="string", length=255, nullable=false)
     */
    private $adress;

    /**
     * @var string
     *
     * @ORM\Column(name="adressCity", type="string", length=255, nullable=false)
     */
    private $adresscity;

    /**
     * @var string
     *
     * @ORM\Column(name="adressCp", type="string", length=255, nullable=false)
     */
    private $adresscp;

    /**
     * @return string
     */
    public function getAdress()
    {
        return $this->adress;
    }

    /**
     * @param string $adress
     */
    public function setAdress($adress)
    {
        $this->adress = $adress;
    }

    /**
     * @return string
     */
    public function getAdresscity()
    {
        return $this->adresscity;
    }

    /**
     * @param string $adresscity
     */
    public function setAdresscity($adresscity)
    {
        $this->adresscity = $adresscity;
    }

    /**
     * @return string
     */
    public function getAdresscp()
    {
        return $this->adresscp;
    }

    /**
     * @param string $adresscp
     */
    public function setAdresscp($adresscp)
    {
        $this->adresscp = $adresscp;
    }

    /**
     * @return string
     */
    public function getAdresscomplement()
    {
        return $this->adresscomplement;
    }

    /**
     * @param string $adresscomplement
     */
    public function setAdresscomplement($adresscomplement)
    {
        $this->adresscomplement = $adresscomplement;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="adressComplement", type="string", length=255, nullable=true)
     */
    private $adresscomplement;

//    /**
//     * @return string
//     */
//    public function getFirstname()
//    {
//        return $this->firstname;
//    }
//
//    /**
//     * @param string $firstname
//     */
//    public function setFirstname($firstname)
//    {
//        $this->firstname = $firstname;
//    }
//
//    /**
//     * @return string
//     */
//    public function getLastname()
//    {
//        return $this->lastname;
//    }
//
//    /**
//     * @param string $lastname
//     */
//    public function setLastname($lastname)
//    {
//        $this->lastname = $lastname;
//    }
//
//    /**
//     * @var string
//     *
//     * @ORM\Column(name="FirstName", type="string", length=255, nullable=false)
//     */
//    private $firstname;
//
//    /**
//     * @var string
//     *
//     * @ORM\Column(name="LastName", type="string", length=255, nullable=false)
//     */
//    private $lastname;

    /**
     * @var string
     *
     * @ORM\Column(name="Email", type="string", length=255, nullable=false)
     */
    private $email;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Graduation", mappedBy="student", cascade={"persist", "remove"})
     */
    private $graduation;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->graduation = new ArrayCollection();
    }


    public function __toString()
    {
        return sprintf('%s %s', $this->lastName, $this->firstName);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Student
     */
    public function setId( $id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return Student
     */
    public function setEmail( $email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGraduation()
    {
        return $this->graduation;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $graduation
     * @return Student
     */
    public function setGraduation(\Doctrine\Common\Collections\Collection $graduation)
    {
        $this->graduation = $graduation;
        return $this;
    }
}

