<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Traits\PersonTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * TechnicalRef
 *
 * @ORM\Table(name="technical_ref", indexes={@ORM\Index(name="FK_TO_company_id", columns={"company_id"})})
 * @ORM\Entity
 */
class TechnicalRef
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Email", type="string", length=255)
     */
    private $email;

    use PersonTrait;

    /**
     * @var \AppBundle\Entity\Company
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Company")
     */
    private $company;


public function __toString()
{
    return sprintf('%s %s', $this->lastName, $this->firstName);
}

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return TechnicalRef
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param Company $company
     * @return TechnicalRef
     */
    public function setCompany(Company $company)
    {
        $this->company = $company;
        return $this;
    }


}

