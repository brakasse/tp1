<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\Collection;

/**
 * Internship
 *
 * @ORM\Table(name="internship")
 * @ORM\Entity
 */
class Internship
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    public function __toString()
    {
        return sprintf('%s %s %s', $this->begin->format('d/m/Y'),
            $this->end->format('d/m/Y'), $this->student);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return \DateTime
     */
    public function getBegin()
    {
        return $this->begin;
    }

    /**
     * @param \DateTime $begin
     */
    public function setBegin($begin)
    {
        $this->begin = $begin;
    }

    /**
     * @return \DateTime
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * @param \DateTime $end
     */
    public function setEnd($end)
    {
        $this->end = $end;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param Company $company
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     * @return PedaRef
     */
    public function getPeda()
    {
        return $this->peda;
    }

    /**
     * @param PedaRef $peda
     */
    public function setPeda($peda)
    {
        $this->peda = $peda;
    }

    /**
     * @return Student
     */
    public function getStudent()
    {
        return $this->student;
    }

    /**
     * @param Student $student
     */
    public function setStudent($student)
    {
        $this->student = $student;
    }

    /**
     * @return TechnicalRef
     */
    public function getTechnical()
    {
        return $this->technical;
    }

    /**
     * @param TechnicalRef $technical
     */
    public function setTechnical($technical)
    {
        $this->technical = $technical;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTech()
    {
        return $this->tech;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $tech
     */
    public function setTech($tech)
    {
        $this->tech = $tech;
    }

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="begin", type="datetime", nullable=false)
     */
    private $begin;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end", type="datetime", nullable=false)
     */
    private $end;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=false)
     */
    private $description;

    /**
     * @var Company
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Company")
     */
    private $company;

    /**
     * @var PedaRef
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\PedaRef")
     */
    private $peda;

    /**
     * @var Student
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Student")
     */
    private $student;

    /**
     * @var TechnicalRef
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\TechnicalRef")
     */
    private $technical;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Technology", cascade="all")
     */
    private $tech;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->begin = new \DateTime();
        $this->end = new \DateTime();

    }
//
//    /**
//     * @return int
//     */
//    public function getId()
//    {
//        return $this->id;
//    }
//
//    /**
//     * @return \DateTime
//     */
//    public function getBegin()
//    {
//        return $this->begin;
//    }
//
//    /**
//     * @param \DateTime $begin
//     * @return Internship
//     */
//    public function setBegin(\DateTime $begin)
//    {
//        $this->begin = $begin;
//        return $this;
//    }
//
//    /**
//     * @return \DateTime
//     */
//    public function getEnd()
//    {
//        return $this->end;
//    }
//
//    /**
//     * @param \DateTime $end
//     * @return Internship
//     */
//    public function setEnd(\DateTime $end)
//    {
//        $this->end = $end;
//        return $this;
//    }
//
//    /**
//     * @return string
//     */
//    public function getDescription()
//    {
//        return $this->description;
//    }
//
//    /**
//     * @param string $description
//     * @return Internship
//     */
//    public function setDescription(string $description)
//    {
//        $this->description = $description;
//        return $this;
//    }
//
//    /**
//     * @return mixed
//     */
//    public function getCompany()
//    {
//        return $this->company;
//    }
//
//    /**
//     * @param mixed $company
//     * @return Internship
//     */
//    public function setCompany($company)
//    {
//        $this->company = $company;
//        return $this;
//    }
//
//    /**
//     * @return mixed
//     */
//    public function getPeda()
//    {
//        return $this->peda;
//    }
//
//    /**
//     * @param mixed $peda
//     * @return Internship
//     */
//    public function setPeda($peda)
//    {
//        $this->peda = $peda;
//        return $this;
//    }
//
//    /**
//     * @return mixed
//     */
//    public function getStudent()
//    {
//        return $this->student;
//    }
//
//    /**
//     * @param mixed $student
//     * @return Internship
//     */
//    public function setStudent($student)
//    {
//        $this->student = $student;
//        return $this;
//    }
//
//    /**
//     * @return mixed
//     */
//    public function getTechnical()
//    {
//        return $this->technical;
//    }
//
//    /**
//     * @param mixed $technical
//     * @return Internship
//     */
//    public function setTechnical($technical)
//    {
//        $this->technical = $technical;
//        return $this;
//    }
//
//    /**
//     * @return mixed
//     */
//    public function getTech()
//    {
//        return $this->tech;
//    }
//
//    /**
//     * @param mixed $tech
//     * @return Internship
//     */
//    public function setTech($tech)
//    {
//        $this->tech = $tech;
//        return $this;
//    }
}

